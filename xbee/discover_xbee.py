from digi.xbee.devices import XBeeDevice
import time
class Discover():
    def __init__(self, xbee):
       self.xbee = xbee # This allows the local xbee device to be passed to the class when called.

    #This callback makes sure to collect the neccesary nodes. This calls out the search after 5 minutes
    def DiscCallback(self, remote): 
        print("New device discovered")
        remote.get_64bit_addr()
        remote.get_node_id()
        remote.get_role()

    def get_remote_devices(self):
        xnet = self.xbee.get_network()
        xnet.add_device_discovered_callback(self.DiscCallback)
        xnet.set_discovery_timeout(5)
        print("Starting discovery process")
        xnet.start_discovery_process()
        while xnet.is_discovery_running():
            time.sleep(0.5)
        tempNodes = xnet.get_devices()
        

        if len(tempNodes) >= 1:
            self.nodes = tempNodes #This passes a list of nodes, which can be indexed into.
            return self.nodes
        else:
            pass
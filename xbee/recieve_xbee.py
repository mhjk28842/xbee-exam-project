from digi.xbee.devices import XBeeDevice
import time
class Receiver():
    
    def __init__(self, xbee):
        self.xbee = xbee

    def datareceived_callback(self, xbee_message): 
        nodeID = xbee_message.remote_device.get_node_id()
        timestamp = time.strftime('%H:%M:%S', time.localtime(xbee_message.timestamp))
        address = xbee_message.remote_device.get_64bit_addr()
        data = xbee_message.data.decode("utf8")
        print("\nReceived data from device %s %s at time %s: %s" % (nodeID, address, timestamp, data))

    def AddCallback(self):
        self.xbee.add_data_received_callback(self.datareceived_callback)
        
from digi.xbee.devices import XBeeDevice

class sendTxtData():
    
    def __init__(self, xbee):
        self.xbee = xbee

    def sendData(self, remote_xbeedevice, data_send): 
        self.xbee.send_data(remote_xbeedevice, data_send)

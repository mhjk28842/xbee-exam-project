from digi.xbee.devices import XBeeDevice
from discover_xbee import Discover
from recieve_xbee import Receiver
from send_xbee import sendTxtData
import sys
import glob
import serial
openPorts = []

class Controller():

    def __init__(self, port, baud):
        self.port = port
        self.baud = baud

    def openlocal(self):
        self.xbee = XBeeDevice(self.port, self.baud)
        self.xbee.open()
    
    def enable_discover(self):
        #this enables the discover class from discover_xbee file and passes the local device to the class.
        self.discover = Discover(self.xbee)

    def enable_sender(self):
        self.sender = sendTxtData(self.xbee)
        
    def getlocalinfo(self, xbeedevice):
        print("Local MAC address:",xbeedevice.get_64bit_addr())
        print("Local node ID:",xbeedevice.get_node_id())
        print("Local role:",xbeedevice.get_role())


if sys.platform.startswith('win'):
    ports = ['COM%s' % (i + 1) for i in range(256)]
elif sys.platform.startswith('linux') or sys.platform.startswith('cygwin'):
    ports = glob.glob('/dev/ttyUSB[A-Za-z0-9]')
else:
    raise EnvironmentError('Unsupported platform')
for port in ports:
    try: #Tries to open the port, if succesful close and append to list
        s = serial.Serial(port)
        s.close()
        openPorts.append(port)
    except (OSError, serial.SerialException): #If port is not open skip it
        pass
print(openPorts) #Gives list of useable ports

for idx, port in enumerate(openPorts):
            print("Active port(s):", port, ":", idx)

ChosenPort = int(input("Choose a port to open with Xbee"))
ChosenPort = openPorts[ChosenPort]

control = Controller(ChosenPort,9600)
control.openlocal()
#initialize controller class and open device with respective port and baudrate.
recieve = Receiver(control.xbee)

#Call to print info on opened local xbee.
control.getlocalinfo(control.xbee)

control.enable_discover() #I call discover to instaniate the class. This passes local xbee to the class
control.discover.get_remote_devices()

control.enable_sender()

try:
    #This for loop puts numbers in front of our found xbee devices
    for idx, device in enumerate(control.discover.nodes):
            print("Remote device(s) discovered:", device, ":", idx)
#If no node is found it will exit the code.
except Exception as e:
    print(e,". Problem finding nodes")
    exit()

#Prompt to pick found node from line 43.
module = int(input("Choose a node to send to\n"))
remote_xbeedevice = control.discover.nodes[module]

RecvMsgInp = input("Attach message callback? [Y]/[N]\n")
if RecvMsgInp.lower == "yes" or "ye" or "y":
    recieve.AddCallback()
elif RecvMsgInp.lower == "no" or "n":
    pass
else:
    print("Not allowed option")
    

while True:
    data_send = input("Write message here: ")
    if data_send == None or "":
        pass
    else:
        control.sender.sendData(remote_xbeedevice, data_send)
